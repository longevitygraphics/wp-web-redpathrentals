<?php

//Custom Post Types
function create_post_type() {

    // SERVICES
    register_post_type( 'service',
        array(
          'labels' => array(
            'name' => __( 'Services' ),
            'singular_name' => __( 'Service' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'services'),
          'show_in_menu'    => 'lg_menu',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    register_taxonomy(
      'service-category',
      'service',
      array(
        'label' => __( 'Category' ),
        'rewrite' => array( 'slug' => 'service-category', "with_front" => false ),
        'hierarchical' => true,
      )
    );

    add_submenu_page(
        'lg_menu',
        __('Service Category'), 
        __('Service Category'), 
        'edit_themes', 
        'edit-tags.php?taxonomy=service-category'
    );

    // LOCATIONS
    register_post_type( 'location',
        array(
          'labels' => array(
            'name' => __( 'Locations' ),
            'singular_name' => __( 'Location' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'location'),
          'show_in_menu'    => 'lg_menu',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );


}
add_action( 'init', 'create_post_type' );

?>