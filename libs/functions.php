<?php

	class lgFunctions{

		private static $instance = null;

		private function __construct(){
			add_shortcode( 'utility-nav', array($this, 'utility_nav') );
			add_action('wp_utility_left',array($this, 'utility_bar'));
			add_action('after_setup_theme', array($this, 'add_template_support'), 16);
			add_action('wp_content_top', array($this, 'featured_banner_top'));
			add_action('wp_content_bottom', array($this, 'service_category_slider_bottom'));
		}

		function utility_bar(){		
			echo do_shortcode('<div class="d-none d-sm-block">[lg-social-media]</div>');
          	$mainMenu = array(
          		// 'menu'              => 'menu-1',
          		'theme_location'    => 'utility-nav',
          		'depth'             => 1,
          		'container'         => 'div'
          	);
          	wp_nav_menu($mainMenu);
		}

		function add_template_support() {
			register_nav_menu( "utility-nav", "Utility Nav Menu(utility-nav)" );
		}

		function service_category_slider_bottom(){
			ob_start(); ?>
			<?php if(is_tax()): ?> 
				<div class="bg-light py-5">
					<div class="container">
						<?php get_template_part("/templates/template-parts/page/service-category-slider"); ?>
					</div>
				</div>
				<div class="bg-white py-5">
					<div class="container">
						<div class="col-12">
							<?php get_template_part("/templates/template-parts/page/home-members"); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<!--<?php get_template_part( '/templates/template-parts/page/content-bottom'); ?> -->
			<?php echo ob_get_clean();
		}

		function featured_banner_top(){
			ob_start(); ?>
			<?php if (!is_page('thank-you')): ?>
				<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?> 
			<?php endif ?>
			<?php echo ob_get_clean();
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFunctions();
		    }
		 
		    return self::$instance;
		}
	}

	lgFunctions::getInstance();

?>