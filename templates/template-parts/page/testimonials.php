<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'lg_testimonial',
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
    	
		<div class="testimonials-list">
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
    	$content = get_field('content');
    	$author = get_field('author');
    ?>
    	
        <div class="py-4">
        	<h2 class="text-dark h3"><?php echo $author; ?></h2>
        	<?php echo $content; ?>
        </div>

        <hr>

		<?php
        endwhile;
        ?>
        </div>

    <?php endif; // End Loop

    wp_reset_query();
?>