<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<?php
		$banner = get_option('lg_option_blog_archive_banner_image');
		$banner_height = get_option('lg_option_blog_archive_banner_height') ? get_option('lg_option_blog_archive_banner_height') : '400px';
		$blog_style = get_option('lg_option_blog_style') ? get_option('lg_option_blog_style') : 'list';
	?>

	<?php if($banner): ?>
		<div class="blog-banner" style="height: <?php echo $banner_height; ?>">
			<img src="<?php echo $banner; ?>">
		</div>
	<?php endif; ?>

	<main class="blog">


		<div class="container">
			<?php
				switch ($blog_style) {
				    case "list":
				        get_template_part( 'templates/template-parts/blog/list');
				        break;
				    case "grid":
				        get_template_part( 'templates/template-parts/blog/grid');
				        break;
				    case "masonary":
				        get_template_part( 'templates/template-parts/blog/masonary');
				        break;
				}
			?>
		</div>
	</main>

<?php get_footer(); ?>