// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){
    	if($('.top-banner .gallery')[0]){
    		$('.top-banner .gallery').slick({
			  arrows: false,
			  autoplay: true,
  			  autoplaySpeed: 5000
			});
    	}

      $('.involvement_slider').slick({
            arrows:false,
            autoplay: true,
            autoplaySpeed: 2800,
            slidesToShow: 6,
            slidesToScroll: 1,
            responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 512,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 420,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            }
            ]
      });
    

      $('#servicecatslider').slick({
        arrows:false,
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 512,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 420,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        ]
      });
    }); 


}(jQuery));